<?php
/**
 * Created by PhpStorm.
 * User: StefanV
 * Date: 11/7/2018
 * Time: 12:43 AM
 */

namespace App\Strategy\Exchange;


use App\Factory\ExchangeFactory;
use Money\Currency;

abstract class AbstractExchangeStrategy implements ExchangeStrategyInterface
{
    /** @var ExchangeFactory */
    protected $exchangeFactory;

    /** @var Currency */
    protected $baseCurrency;

    /** @var Currency */
    protected $counterCurrency;

    /**
     * @param ExchangeFactory $exchangeFactory
     * @return ExchangeStrategyInterface
     * @codeCoverageIgnore
     */
    public function setExchangeFactory(ExchangeFactory $exchangeFactory): ExchangeStrategyInterface
    {
        $this->exchangeFactory = $exchangeFactory;

        return $this;
    }

    /**
     * @param Currency $currency
     * @return ExchangeStrategyInterface
     * @codeCoverageIgnore
     */
    public function setBaseCurrency(Currency $currency): ExchangeStrategyInterface
    {
        $this->baseCurrency = $currency;

        return $this;
    }

    /**
     * @param Currency $currency
     * @return ExchangeStrategyInterface
     * @codeCoverageIgnore
     */
    public function setCounterCurrency(Currency $currency): ExchangeStrategyInterface
    {
        $this->counterCurrency = $currency;

        return $this;
    }
}
