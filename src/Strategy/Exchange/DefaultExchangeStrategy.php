<?php
namespace App\Strategy\Exchange;

use Money\CurrencyPair;

class DefaultExchangeStrategy extends AbstractExchangeStrategy
{
    /**
     * @return CurrencyPair
     */
    public function getQuote(): CurrencyPair
    {
        $exchange = $this->exchangeFactory->createIndirectExchange(
            $this->exchangeFactory->createReversedCurrenciesExchange($this->exchangeFactory->createFixedExchange())
        );

        return $exchange->quote($this->baseCurrency, $this->counterCurrency);
    }
}
