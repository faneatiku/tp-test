<?php
namespace App\Strategy\Exchange;

use Money\CurrencyPair;

class SmallestExchangeRateStrategy extends AbstractExchangeStrategy
{
    /**
     * @return CurrencyPair
     */
    public function getQuote(): CurrencyPair
    {
        $exchange = $this->exchangeFactory->createIndirectExchange(
            $this->exchangeFactory->createReversedCurrenciesExchange($this->exchangeFactory->createFixedExchange())
        );

        $quote = $exchange->quote($this->baseCurrency, $this->counterCurrency);

        $reversedQuote = $exchange->quote($this->counterCurrency, $this->baseCurrency);

        return $quote->getConversionRatio() <= $reversedQuote->getConversionRatio() ? $quote : $reversedQuote;
    }
}