<?php
namespace App\Strategy\Exchange;

use App\Factory\ExchangeFactory;
use Money\Currency;
use Money\CurrencyPair;

interface ExchangeStrategyInterface
{
    /**
     * @param ExchangeFactory $exchangeFactory
     * @return ExchangeStrategyInterface
     */
    public function setExchangeFactory(ExchangeFactory $exchangeFactory): ExchangeStrategyInterface;

    /**
     * @param Currency $currency
     * @return ExchangeStrategyInterface
     */
    public function setBaseCurrency(Currency $currency): ExchangeStrategyInterface;

    /**
     * @param Currency $currency
     * @return ExchangeStrategyInterface
     */
    public function setCounterCurrency(Currency $currency): ExchangeStrategyInterface;

    /**
     * @return CurrencyPair
     */
    public function getQuote(): CurrencyPair;
}
