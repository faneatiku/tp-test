<?php
namespace App\Controller;

use App\Factory\ExchangeStrategyFactory;
use App\Service\ExchangeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends AbstractController
{
    /**
     * @return JsonResponse
     */
    public function index()
    {
        return new JsonResponse(['ping' => 'pong']);
    }

    /**
     * @param Request $request
     * @param string $baseCurrency
     * @param string $counterCurrency
     * @param ExchangeService $exchangeService
     * @return JsonResponse
     */
    public function exchange(Request $request, string $baseCurrency, string $counterCurrency, ExchangeService $exchangeService)
    {
        try {
            return new JsonResponse(
                $exchangeService->getExchangeRate(
                    $baseCurrency,
                    $counterCurrency,
                    $request->query->get('option', ExchangeStrategyFactory::DEFAULT_EXCHANGE_STRATEGY)
                )
            );
        } catch (\Throwable $ex) {
            return new JsonResponse(['error' => $ex->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
