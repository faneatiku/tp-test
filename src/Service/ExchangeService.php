<?php
namespace App\Service;

use App\Factory\CurrencyFactory;
use App\Factory\ExchangeFactory;
use App\Factory\ExchangeStrategyFactory;
use Money\CurrencyPair;

class ExchangeService
{
    /**
     * @var CurrencyFactory
     */
    private $currencyFactory;

    /**
     * @var ExchangeFactory
     */
    private $exchangeFactory;

    /**
     * @var ExchangeStrategyFactory
     */
    private $exchangeStrategyFactory;

    /**
     * @return ExchangeStrategyFactory
     */
    public function getExchangeStrategyFactory(): ExchangeStrategyFactory
    {
        return $this->exchangeStrategyFactory;
    }

    /**
     * @param ExchangeStrategyFactory $exchangeStrategyFactory
     * @return $this
     * @required
     */
    public function setExchangeStrategyFactory(ExchangeStrategyFactory $exchangeStrategyFactory): self
    {
        $this->exchangeStrategyFactory = $exchangeStrategyFactory;
        return $this;
    }

    /**
     * @return CurrencyFactory
     * @codeCoverageIgnore
     */
    public function getCurrencyFactory(): CurrencyFactory
    {
        return $this->currencyFactory;
    }

    /**
     * @param CurrencyFactory $currencyFactory
     * @return $this
     * @required
     * @codeCoverageIgnore
     */
    public function setCurrencyFactory(CurrencyFactory $currencyFactory): self
    {
        $this->currencyFactory = $currencyFactory;
        return $this;
    }

    /**
     * @return ExchangeFactory
     * @codeCoverageIgnore
     */
    public function getExchangeFactory(): ExchangeFactory
    {
        return $this->exchangeFactory;
    }

    /**
     * @param ExchangeFactory $exchangeFactory
     * @return $this
     * @required
     * @codeCoverageIgnore
     */
    public function setExchangeFactory(ExchangeFactory $exchangeFactory): self
    {
        $this->exchangeFactory = $exchangeFactory;
        return $this;
    }

    /**
     * @param string $baseCurrency
     * @param string $counterCurrency
     * @param string $strategy
     * @return CurrencyPair
     */
    public function getExchangeRate(string $baseCurrency, string $counterCurrency, string $strategy): CurrencyPair
    {
        $exchangeStrategy = $this->getExchangeStrategyFactory()->getStrategy($strategy);

        $baseCurrency       = $this->getCurrencyFactory()->createFromCode($baseCurrency);
        $counterCurrency    = $this->getCurrencyFactory()->createFromCode($counterCurrency);

        $exchangeStrategy->setExchangeFactory($this->getExchangeFactory())
            ->setBaseCurrency($baseCurrency)
            ->setCounterCurrency($counterCurrency);

        return $exchangeStrategy->getQuote();
    }
}
