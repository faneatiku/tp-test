<?php
namespace App\Factory;

use Money\Currencies\CurrencyList;
use Money\Currency;

class CurrencyFactory
{
    public const BTC_CODE = 'BTC';
    public const BCH_CODE = 'BCH';
    public const USD_CODE = 'USD';
    public const EUR_CODE = 'EUR';

    //code => subunit(decimals)
    private const CURRENCIES_MAP = [
        self::USD_CODE => 2,
        self::EUR_CODE => 2,
        self::BTC_CODE => 8,
        self::BCH_CODE => 8
    ];

    /**
     * @return array
     */
    public function getCurrenciesMap(): array
    {
        return self::CURRENCIES_MAP;
    }

    /**
     * @return CurrencyList
     */
    public function getCurrenciesList(): CurrencyList
    {
        return new CurrencyList($this->getCurrenciesMap());
    }

    /**
     * @param string $code
     * @return Currency
     */
    public function createFromCode(string $code): Currency
    {
        $code = strtoupper($code);
        $currenciesMap = $this->getCurrenciesMap();

        if (!isset($currenciesMap[$code])) {
            throw new \Money\Exception\UnknownCurrencyException(sprintf(
                'Could not create currency from code [%s]. Accepted values [%s]',
                $code,
                implode(',', array_keys($currenciesMap))
            ));
        }

        return new Currency($code);
    }
}
