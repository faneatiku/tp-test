<?php
namespace App\Factory;

use Money\Currencies\CurrencyList;
use Money\Exchange\FixedExchange;
use Money\Exchange\IndirectExchange;
use Money\Exchange\ReversedCurrenciesExchange;

class ExchangeFactory
{
    private const DEFAULT_EXCHANGE_PAIRS_MAP = [
        CurrencyFactory::BTC_CODE => [
            CurrencyFactory::USD_CODE => 6200,
        ],
        CurrencyFactory::BCH_CODE => [
            CurrencyFactory::USD_CODE => 500,
        ],
        CurrencyFactory::USD_CODE => [
            CurrencyFactory::EUR_CODE => 0.8695652175,
        ],
        CurrencyFactory::EUR_CODE => [
            CurrencyFactory::USD_CODE => 1.15,
        ],
    ];

    /** @var CurrencyFactory */
    protected $currencyFactory;

    /**
     * @param array $pairsMap
     * @return FixedExchange
     */
    public function createFixedExchange(array $pairsMap = []): FixedExchange
    {
        if (empty($pairsMap)) {
            $pairsMap = $this->getDefaultExchangePairsMap();
        }

        return new FixedExchange($pairsMap);
    }

    /**
     * @param FixedExchange $fixedExchange
     * @return ReversedCurrenciesExchange
     */
    public function createReversedCurrenciesExchange(FixedExchange $fixedExchange): ReversedCurrenciesExchange
    {
        return new ReversedCurrenciesExchange($fixedExchange);
    }

    /**
     * @param ReversedCurrenciesExchange $reversedCurrenciesExchange
     * @param CurrencyList $currencyList
     * @return IndirectExchange
     */
    public function createIndirectExchange(ReversedCurrenciesExchange $reversedCurrenciesExchange, ?CurrencyList $currencyList = null): IndirectExchange
    {
        if (null === $currencyList) {
            $currencyList = $this->getCurrencyFactory()->getCurrenciesList();
        }

        return new IndirectExchange($reversedCurrenciesExchange, $currencyList);
    }

    /**
     * Should return exchange pairs map from storage.
     *
     * @return array
     */
    protected function getDefaultExchangePairsMap(): array
    {
        return self::DEFAULT_EXCHANGE_PAIRS_MAP;
    }

    /**
     * @param CurrencyFactory $currencyFactory
     * @return $this
     * @required
     * @codeCoverageIgnore
     */
    public function setCurrencyFactory(CurrencyFactory $currencyFactory): self
    {
        $this->currencyFactory = $currencyFactory;
        return $this;
    }

    /**
     * @return CurrencyFactory
     * @codeCoverageIgnore
     */
    protected function getCurrencyFactory(): CurrencyFactory
    {
        return $this->currencyFactory;
    }
}
