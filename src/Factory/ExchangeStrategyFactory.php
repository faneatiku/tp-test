<?php
namespace App\Factory;

use App\Strategy\Exchange\DefaultExchangeStrategy;
use App\Strategy\Exchange\ExchangeStrategyInterface;
use App\Strategy\Exchange\SmallestExchangeRateStrategy;

class ExchangeStrategyFactory
{
    const SMALLEST_EXCHANGE_STRATEGY = 'smallest';
    const DEFAULT_EXCHANGE_STRATEGY = 'default';

    /**
     * @param null|string $option
     * @return ExchangeStrategyInterface
     */
    public function getStrategy(?string $option = null): ExchangeStrategyInterface
    {
        switch ($option) {
            case self::SMALLEST_EXCHANGE_STRATEGY:
                return new SmallestExchangeRateStrategy();
                break;
            default:
                return new DefaultExchangeStrategy();
        }
    }
}
