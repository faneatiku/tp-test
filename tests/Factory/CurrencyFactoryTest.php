<?php
namespace App\Tests\Service;

use App\Factory\CurrencyFactory;
use Money\Currencies\CurrencyList;
use Money\Currency;
use Money\Exception\UnknownCurrencyException;
use PHPUnit\Framework\TestCase;

class CurrencyFactoryTest extends TestCase
{
    /** @var CurrencyFactory */
    private $factory;

    public function setUp()
    {
        parent::setUp();
        $this->factory = new CurrencyFactory();
    }

    public function testGetCurrenciesMap()
    {
        $currenciesMap = $this->factory->getCurrenciesMap();
        $this->assertTrue(!empty($currenciesMap));
        $this->assertArrayHasKey(CurrencyFactory::USD_CODE, $currenciesMap);
    }

    public function testGetCurrenciesList()
    {
        $map = array_values(array_flip($this->factory->getCurrenciesMap()));
        $list = $this->factory->getCurrenciesList();

        $this->assertInstanceOf(CurrencyList::class, $list);
        $this->assertTrue($list->contains(new Currency($map[0])));
    }

    public function testCreateFromCodeWithValidCode()
    {
        $map = array_values(array_flip($this->factory->getCurrenciesMap()));
        $result = $this->factory->createFromCode($map[0]);
        $this->assertInstanceOf(Currency::class, $result);
    }

    public function testCreateFromCodeThrowsException()
    {
        $map = array_values(array_flip($this->factory->getCurrenciesMap()));
        $this->expectException(UnknownCurrencyException::class);
        $this->expectExceptionMessage(
            sprintf(
                'Could not create currency from code [%s]. Accepted values [%]',
                'MOCK',
                implode(',', $map)
            )
        );
        $this->factory->createFromCode('MOCK');
    }
}
