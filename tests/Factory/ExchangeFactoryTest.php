<?php
namespace App\Tests\Service;

use App\Factory\CurrencyFactory;
use App\Factory\ExchangeFactory;
use Money\Exchange\FixedExchange;
use Money\Exchange\IndirectExchange;
use Money\Exchange\ReversedCurrenciesExchange;
use PHPUnit\Framework\TestCase;

class ExchangeFactoryTest extends TestCase
{
    /** @var ExchangeFactory */
    private $exchangeFactory;

    public function setUp()
    {
        parent::setUp();

        $this->exchangeFactory = new ExchangeFactory();
        $this->exchangeFactory->setCurrencyFactory(new CurrencyFactory());
    }

    public function testCreateFixedExchange()
    {
        $result = $this->exchangeFactory->createFixedExchange();

        $this->assertInstanceOf(FixedExchange::class, $result);
    }

    public function testCreateReversedCurrenciesExchange()
    {
        $result = $this->exchangeFactory->createReversedCurrenciesExchange($this->exchangeFactory->createFixedExchange());

        $this->assertInstanceOf(ReversedCurrenciesExchange::class, $result);
    }

    public function testCreateIndirectExchange()
    {
        $result = $this->exchangeFactory->createIndirectExchange(
            $this->exchangeFactory->createReversedCurrenciesExchange($this->exchangeFactory->createFixedExchange())
        );
        $this->assertInstanceOf(IndirectExchange::class, $result);
    }
}
