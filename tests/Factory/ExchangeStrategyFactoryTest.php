<?php
namespace App\Tests\Factory;

use App\Factory\ExchangeStrategyFactory;
use App\Strategy\Exchange\DefaultExchangeStrategy;
use App\Strategy\Exchange\SmallestExchangeRateStrategy;
use PHPUnit\Framework\TestCase;

class ExchangeStrategyFactoryTest extends TestCase
{
    /**
     * @return array
     */
    public function getStrategyDataProvider()
    {
        return [
            'case default' => [
                '$option' => 'default',
                '$expectedClass' => DefaultExchangeStrategy::class
            ],
            'case smallest' => [
                '$option' => 'smallest',
                '$expectedClass' => SmallestExchangeRateStrategy::class
            ]
        ];
    }

    /**
     * @param $option
     * @param $expectedClass
     * @dataProvider getStrategyDataProvider
     */
    public function testGetStrategy($option, $expectedClass)
    {
        $factory = new ExchangeStrategyFactory();
        $result = $factory->getStrategy($option);

        $this->assertInstanceOf($expectedClass, $result);
    }
}
