<?php
namespace App\Tests\Service;

use App\Factory\CurrencyFactory;
use App\Factory\ExchangeFactory;
use App\Factory\ExchangeStrategyFactory;
use App\Service\ExchangeService;
use Money\Currency;
use Money\CurrencyPair;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ExchangeServiceTest extends TestCase
{
    /** @var ExchangeService */
    private $exchangeService;

    /** @var ExchangeFactory */
    private $exchangeFactory;

    /** @var CurrencyFactory|MockObject */
    private $currencyFactoryMock;

    public function setUp()
    {
        parent::setUp();
        $this->exchangeService = new ExchangeService();
        $this->exchangeFactory = new ExchangeFactory();
        $this->currencyFactoryMock = new CurrencyFactory();

        $this->exchangeFactory->setCurrencyFactory($this->currencyFactoryMock);

        $this->exchangeService->setCurrencyFactory($this->currencyFactoryMock);
        $this->exchangeService->setExchangeFactory($this->exchangeFactory);
        $this->exchangeService->setExchangeStrategyFactory(new ExchangeStrategyFactory());
    }

    public function getExchangeRateIsCurrencyPairDataProvider()
    {
        return [
            'default' => [
                '$exchangeStrategy' => 'default'
            ],
            'smallest' => [
                '$exchangeStrategy' => 'smallest'
            ]
        ];
    }

    /**
     * @param $exchangeStrategy
     * @dataProvider getExchangeRateIsCurrencyPairDataProvider
     */
    public function testGetExchangeRateIsCurrencyPair($exchangeStrategy)
    {
        $baseCurrency = new Currency(CurrencyFactory::USD_CODE);
        $counterCurrency = new Currency( CurrencyFactory::EUR_CODE);

        $result = $this->exchangeService->getExchangeRate(
            CurrencyFactory::USD_CODE,
            CurrencyFactory::EUR_CODE,
            $exchangeStrategy
        );

        $this->assertInstanceOf(CurrencyPair::class, $result);
        $this->assertEquals($baseCurrency, $result->getBaseCurrency());
        $this->assertEquals($counterCurrency, $result->getCounterCurrency());
    }
}
